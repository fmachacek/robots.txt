V této úloze musí řešitel najít flag, který se nachází v zazipovaném souboru. Na úvodní stránce se dočte, že soubor je chárněn proti botům (je zakázáno zobrazení souboru ve vyhledávání).
Je to nápověda, soubory které se nemají zobrazovat ve výsledcích hledání se nachází v souboru robots.txt. Když na tuto stránku řešitel vkročí, uvítá ho několik "odkazů" na zazipované soubory, ale pouze jeden z nich jde stáhnout.
Konkrétně pak soubor /bak/b7e678c97d443b228bf385b7c44dd3aadc3b92693af817cb072ef3d1a4963defdcd25f28dbf56cb85e1e3caddb2de900.zip. V tomto souboru najde uživatel soubor "prapor.txt", který je i flagou, ale je zašifrován pomocí base64.

flag: flag{gotcha}